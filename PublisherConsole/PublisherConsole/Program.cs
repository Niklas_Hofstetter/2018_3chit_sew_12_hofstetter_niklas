﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            FAZPublisher publisher = new FAZPublisher();
            publisher.Subscribe(new FamFischer());
            FamMeier Meier = new FamMeier();
            publisher.Subscribe(Meier);

            publisher.setActnews(new News("Unglaublich!"));
            publisher.Unsubscribe(Meier);
            publisher.setActnews(new News("WOW!"));
        }
    }
}
