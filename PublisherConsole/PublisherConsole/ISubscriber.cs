﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherConsole
{
    public interface ISubscriber
    {
        void ReceiveNews(News news);

    }
    class FamFischer : ISubscriber
    {
        
        public void ReceiveNews(News news)
        {
            Console.WriteLine("Familie Fischer hat die Zeitung erhalten: " + news.getTitel());
        }
    }
    class FamMeier : ISubscriber
    {
        
        public void ReceiveNews(News news)
        {
            Console.WriteLine("Familie Meier hat die Zeitung erhalten: " + news.getTitel());
        }
    }
}
