﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherConsole
{
    public class News
    {
        private string titel;

        public News(string titel)
        {
            this.titel = titel;
        }

        public string getTitel()
        {
            return titel;
        }
    }
}
