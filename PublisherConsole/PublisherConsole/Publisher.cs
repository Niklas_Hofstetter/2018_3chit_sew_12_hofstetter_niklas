﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherConsole
{
    public abstract class Publisher
    {
        private List<ISubscriber> sublist = new List<ISubscriber>();

        public void Subscribe(ISubscriber sub)
        {
            sublist.Add(sub);
        }

        public void Unsubscribe(ISubscriber sub)
        {
            sublist.Remove(sub);
        }

        public void spreadNews(News news)
        {
            foreach (ISubscriber abonnent in sublist)
            {
                abonnent.ReceiveNews(news);
            }
        }
    }
    public class FAZPublisher : Publisher
    {

        private News actnews;

        public void setActnews(News aktuelleZeitung)
        {
            this.actnews = aktuelleZeitung;
            //Nach dem einen neue Zeitung gesetzt wurde, werden alle Abonnenten benachrichtigt. 
            spreadNews(actnews);
        }

        public News getActnews()
        {
            return actnews;
        }
    }
}
