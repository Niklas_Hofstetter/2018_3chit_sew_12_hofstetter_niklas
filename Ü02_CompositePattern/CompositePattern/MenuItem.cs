﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePattern
{
    public class MenuItem : MenuComponent
    {
        String name;
        String description;
        bool vegetarian;
        double price;

        public MenuItem(string n, string d, double p, bool v)
        {
            name = n;
            description = d;
            price = p;
            vegetarian = v;
        }
        public IEnumerator CreateEnumerator()
        {
            return new NullEnumerator();
        }

        public void Add(MenuComponent m)
        {
            throw new NotImplementedException();
        }

        public MenuComponent getChild(int i)
        {
            throw new NotImplementedException();
        }

        public string getDescription()
        {
            return description;
        }

        public string getName()
        {
            return name;
        }

        public double getPrice()
        {
            return price;
        }

        public bool isVegetarian()
        {
            return vegetarian;
        }

        public void Print()
        {
            Console.WriteLine(name + " " + price + " " + description);
        }

        public void Remove(MenuComponent m)
        {
            throw new NotImplementedException();
        }
    }
}
