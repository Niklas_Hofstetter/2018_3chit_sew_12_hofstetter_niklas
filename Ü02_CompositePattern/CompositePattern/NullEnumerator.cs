﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePattern
{
    class NullEnumerator : IEnumerator
    {
        object IEnumerator.Current => throw new NotImplementedException();

        public bool MoveNext()
        {
            return false;
        }

        public void Reset()
        {
        }
    }
}
