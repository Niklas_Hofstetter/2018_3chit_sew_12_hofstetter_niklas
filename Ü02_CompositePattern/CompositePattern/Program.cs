﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Test();
        }
        static void Test()
        {
            Menu m = new Menu("Menu", "Gutes Essen");
            m.Add(new Menu("Fleisch", "Sehr gutes Essen"));
            m.getChild(0).Add(new MenuItem("Faschiertes", "gut gebraten", 10.3, false));
            m.getChild(0).Add(new MenuItem("Schweinsbraten", "lecker!!!", 18.3, false));
            m.getChild(0).Add(new MenuItem("Surbraten", "besonders gut!", 21.76, false));
            m.Add(new Menu("Vegetarisch", "Ohne Fleisch"));
            m.getChild(1).Add(new MenuItem("Sojabohnen", "geröstet", 29.76, true));
            m.getChild(1).Add(new MenuItem("Fake Fleisch", "ist Fleisch wir sagen aber, dass es keines ist", 5.76, true));

            m.Print();
        }
    }
}
