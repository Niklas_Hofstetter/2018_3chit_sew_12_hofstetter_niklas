﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePattern
{
    interface MenuComponent
    {
        string getName();
        string getDescription();
        double getPrice();
        bool isVegetarian();
        void Print();
        void Add(MenuComponent m);
        void Remove(MenuComponent m);
        MenuComponent getChild(int i);
    }
}
