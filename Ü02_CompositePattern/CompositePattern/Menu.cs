﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePattern
{
    class Menu : MenuComponent
    {
        List<MenuComponent> Items { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Menu(string n, string d)
        {
            Items = new List<MenuComponent>();
            Name = n;
            Description = d;
        }

        public IEnumerator CreateEnumerator()
        {
            return Items.GetEnumerator();
        }

        public string getName()
        {
            return Name;
        }

        public string getDescription()
        {
            return Description;
        }

        public double getPrice()
        {
            throw new NotImplementedException();
        }

        public bool isVegetarian()
        {
            throw new NotImplementedException();
        }

        public void Print()
        {
            Console.WriteLine(Name + " " + Description);

            foreach (MenuComponent men in Items)
            {
                men.Print();
            }
        }

        public void Add(MenuComponent m)
        {
            Items.Add(m);
        }

        public void Remove(MenuComponent m)
        {
            Items.Remove(m);
        }

        public MenuComponent getChild(int i)
        {
            return Items[i];
        }
    }
}
