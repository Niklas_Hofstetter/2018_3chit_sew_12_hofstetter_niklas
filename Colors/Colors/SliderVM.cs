﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Drawing;
using System.Windows.Media;

namespace Colors
{
    class SliderVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private int redValue;
        public int RedValue
        {
            get { return redValue; }
            set {
                if (value > 255)
                {
                    redValue = 255;
                }
                else if (value < 0)
                {
                    redValue = 0;
                }
                else
                {
                    redValue = value;
                }
                OnPropertychanged();
                OnPropertychanged("BackgroundColor");
            }
        }
        private int greenValue;
        public int GreenValue
        {
            get { return greenValue; }
            set {
                if (value > 255)
                {
                    greenValue = 255;
                }
                else if (value < 0)
                {
                    greenValue = 0;
                }
                else
                {
                    greenValue = value;
                }
                OnPropertychanged();
                OnPropertychanged("BackgroundColor");
            }
        }
        private int blueValue;
        public int BlueValue
        {
            get { return blueValue; }
            set {
                if (value > 255)
                {
                    blueValue = 255;
                }
                else if (value < 0)
                {
                    blueValue = 0;
                }
                else
                {
                    blueValue = value;
                }
                OnPropertychanged();
                OnPropertychanged("BackgroundColor");
            }
        }
        public ICommand RCCommand { get; set; }

        public SliderVM()
        {
            redValue = 0;
            greenValue = 0;
            blueValue = 0;
            RCCommand = new RandomCCommands(this);
        }

        public string BackgroundColor
        {
            get
            {
                return ColorTranslator.ToHtml(Color.FromArgb(redValue, greenValue, blueValue));
            }
        }

        public void OnPropertychanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
