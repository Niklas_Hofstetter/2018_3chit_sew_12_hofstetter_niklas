﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Colors
{
    class RandomCCommands : ICommand
    {
        public event EventHandler CanExecuteChanged;

        Random r;
        private SliderVM SliderVM;

        public RandomCCommands(SliderVM SliderVM)
        {
            this.SliderVM = SliderVM;
            r = new Random();
            SliderVM.PropertyChanged += delegate { CanExecuteChanged(this, EventArgs.Empty); };
        }


        public bool CanExecute(object parameter)
        {
            return (SliderVM != null);
        }

        public void Execute(object parameter)
        {
            SliderVM.RedValue = r.Next(0, 256);
            SliderVM.GreenValue = r.Next(0, 256);
            SliderVM.BlueValue = r.Next(0, 256);
        }
    }
}
