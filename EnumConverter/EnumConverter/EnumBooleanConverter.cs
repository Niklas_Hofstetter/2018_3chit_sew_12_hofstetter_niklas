﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace EnumConverter
{
    public enum EImportance { TRIVIAL, REGULAR, IMPORTANT };
    class EnumBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //checks if Selection from RadioButtonCheckBoxVM has the same value
            //as the ConverterParameter. Returns true or false
            // return ((Enum)value).HasFlag((Enum)parameter);
            return ((EImportance)value == (EImportance)parameter);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //If the radiobutton is checked, it returns the ConverterParameter
            //return value.Equals(true) ? parameter : Binding.DoNothing;
            return ((bool)value == true) ? parameter : null;
        }
    }    class EnumIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //return (int)value;
            EImportance e = (EImportance)value;
            return System.Convert.ToInt32(value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (EImportance)System.Convert.ToInt32(value);
        }
    }
}
