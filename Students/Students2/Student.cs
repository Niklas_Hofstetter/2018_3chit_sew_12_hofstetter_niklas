﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Students2
{
    class Student
    {
        public string Firstname
        {
            get; private set;
        }
        public string LastName
        {
            get; private set;
        }
        public int Age
        {
            get; private set;
        }

        public Student(string firstname, string lastname, int age)
        {
            this.Firstname = firstname;
            this.LastName = lastname;
            this.Age = age;
        }

        public void Save(string path, Student s)
        {
            StreamWriter sw = new StreamWriter("student.csv", true);
            sw.WriteLine(s.ToCSV());
            sw.Close();
        }

        private char ToCSV()
        {
            throw new NotImplementedException();
        }

        public List<Student> Read(string path)
        {
            StreamReader sr = new StreamReader(path);
            List<Student> students = new List<Student>();
            string line;
            while ((line=sr.ReadLine())!=null)
            {
                string[] splittedLine = line.Split(';');
                students.Add(new Student(splittedLine[0], splittedLine[1], Convert.ToInt32(splittedLine[2], (Classes)Enum.Parse(typeof(Classes)), splittedLine[3])));
            }
            sr.Close();
            return students;
        }
    }
}
